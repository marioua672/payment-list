﻿using PaymentsList.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentsList.BusinessLogic.Interfaces
{
    public interface IExpenseService
    {
        Task<IEnumerable<ExpenceHeader>> GetGroupsAsync();

    }
}
