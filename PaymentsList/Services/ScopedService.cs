﻿using System;

namespace PaymentsList.API.Services
{
    public class ScopedService
    {
        public int Id { get; }
        public ScopedService()
        {
            Id = new Random().Next();
        }
    }
}
