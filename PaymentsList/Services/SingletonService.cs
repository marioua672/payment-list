﻿using System;

namespace PaymentsList.API.Services
{
    public class SingletonService
    {
        public int Id { get; }
        public SingletonService()
        {
            Id = new Random().Next();
        }
    }
}
