﻿using System;

namespace PaymentsList.API.Services
{
    public class TransientService
    {
        public int Id { get; }
        public TransientService()
        {
            Id = new Random().Next();
        }
    }
}
