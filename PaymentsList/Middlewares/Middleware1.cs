﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace PaymentsList.API.Middlewares
{
    public class Middleware1
    {
        private readonly RequestDelegate _next;

        public Middleware1(RequestDelegate next)
        {
            _next = next;
        }



        public async Task InvokeAsync(HttpContext context)
        {
             try
             {
                _next();
             }
             catch(Exception e)
             {

             }
        } 
    }
}
 